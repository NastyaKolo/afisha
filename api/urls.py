from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions


from api.views import (EventDetailsView, CreateEventView,
                       UpdateEventView, DeleteEventView)

app_name = 'api'

schema_view = get_schema_view(
   openapi.Info(
      title="Afisha|API docs",
      default_version='v1',
      description="API description",
      terms_of_service="https://www.google.com/policies/terms/"
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)


urlpatterns = [
    path("docs/", schema_view.with_ui("swagger", cache_timeout=0), name="docs"),
    path("auth/", include("djoser.urls")),
    path("auth/jwt/", include("djoser.urls.jwt")),

    path('event/<pk>', EventDetailsView.as_view(), name='event_details'),

    path('create/', CreateEventView.as_view(), name='create'),
    path('update/<pk>', UpdateEventView.as_view(), name='update'),
    path('delete/<pk>', DeleteEventView.as_view(), name='delete')

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
