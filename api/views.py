from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import (RetrieveAPIView, ListAPIView, DestroyAPIView,
                                     CreateAPIView, UpdateAPIView)

from events.models import Event, Category, Location
from api.serializers import EventDetailSerializer

import datetime


class EventDetailsView(RetrieveAPIView):
    queryset = Event.objects.all()
    serializer_class = EventDetailSerializer

    def get_object(self):
        pk = self.kwargs.get('pk')
        event = Event.objects.get(id=pk)
        return event


class CreateEventView(CreateAPIView):
    queryset = Event.objects.all()
    serializer_class = EventDetailSerializer

    def perform_create(self, serializer):
        return serializer.save()


class UpdateEventView(UpdateAPIView):
    queryset = Event.objects.all()
    serializer_class = EventDetailSerializer

    def get_object(self):
        pk = self.kwargs.get('pk')
        event = Event.objects.get(id=pk)
        return event


class DeleteEventView(DestroyAPIView):
    queryset = Event.objects.all()
    serializer_class = EventDetailSerializer

    def get_object(self):
        pk = self.kwargs.get('pk')
        event = Event.objects.get(id=pk)
        return event
