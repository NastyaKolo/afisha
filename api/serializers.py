from rest_framework.serializers import ModelSerializer
from rest_framework.viewsets import ModelViewSet

from users.models import CustomUser
from events.models import Event, Location, Category


class LocationSerializer(ModelSerializer):
    class Meta:
        model = Location
        fields = ['id', 'name', 'city']


class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'name']


class EventDetailSerializer(ModelSerializer):
    category = CategorySerializer(read_only=True)
    location = LocationSerializer(read_only=True)

    class Meta:
        model = Event
        fields = ['id', 'name', 'date_time', 'price', 'location', 'category']
