FROM python:3.10

RUN apt update

RUN mkdir /afisha

WORKDIR /afisha

COPY ./ ./
COPY ./commands ./commands

RUN python -m pip install --upgrade pip
RUN pip install -r requirements.txt

CMD ["bash"]
