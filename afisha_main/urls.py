"""afisha_main URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from events.views import IndexPage, ContactsPage
from tasks_celery.view import send_usd_rate


urlpatterns = [
    path('admin/', admin.site.urls),
    path('contacts', ContactsPage.as_view(), name='contacts'),
    path('', IndexPage.as_view(), name='index'),

    path('api/', include('api.urls')),

    path('events/', include('events.urls')),
    path('users/', include('users.urls')),

    path('current_usd_rate', send_usd_rate, name='usd_rate')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
