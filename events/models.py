from django.db import models
from django.utils.translation import ugettext as _
import uuid
import random
import datetime
from django.utils import timezone

from djmoney.models.fields import MoneyField
from django.core.validators import MaxValueValidator, MinValueValidator

WEEK_IN_SECONDS = 604800
YEAR_IN_SECONDS = 31449600
DEFAULT_DESCRIPTION = 'Lorem ipsum dolor sit amet, consectetur adipisic elit. Sed voluptate nihil eumester ' \
                      'consectetur similiqu consectetur. Lorem ipsum dolor sit amet, consectetur adipisic ' \
                      'elit. Et, consequuntur, modi mollitia corporis ipsa voluptate corrupti.'


# Create your models here.
class Event(models.Model):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False)

    name = models.CharField(max_length=200, blank=False, null=False, default='event1')
    description = models.TextField(max_length=1000, blank=True, default=DEFAULT_DESCRIPTION)
    media = models.ImageField(upload_to='event_images', null=True, blank=True,
                              default='event_images/default.jpeg')
    
    date_time = models.DateTimeField(blank=False)
    price = MoneyField(max_digits=10, decimal_places=2, null=True, default_currency='UAH')
    tickets_amount = models.IntegerField(blank=True, null=True, validators=[MinValueValidator(1)])

    location = models.ForeignKey(to='events.Location', related_name='location',
                                 on_delete=models.SET_NULL, null=True)
    category = models.ForeignKey(to='events.Category', related_name='category', 
                                 on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.name

    @classmethod
    def generate_events(cls, count):
        EVENT_NAMES = ['Coldplay', 'Inhaler', 'Luna', 'Woodkid', 'Splin', 'Contemporary Ukrainian photography',
                       'Non-stop Painting', 'Amazing Stories of Crimea', 'Instant time exhibition',
                       'Interior of the Year 2022', 'Sziget 2022', 'Lollapalooza Stockholm 2022',
                       'Rock Werchter 2022', 'Atlas Weekend', 'Underground Standup', 'Improv Live Show',]
        for _ in range(count):
            new_event = cls(
                name=random.choice(EVENT_NAMES),
                date_time=timezone.now() + datetime.timedelta(seconds=random.randint(WEEK_IN_SECONDS, 
                                                                                     YEAR_IN_SECONDS)),
                price=random.randrange(60, 20000, 1),
                tickets_amount=random.randint(2, 500),
                location_id=Category.get_random())  # FOREIGN KEY constraint failed
        # new_event.save()
            

class Category(models.Model):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False)
    
    name = models.CharField(max_length=200, blank=False, null=False, default='events_set1')
    description = models.TextField(max_length=1000, blank=True, default=DEFAULT_DESCRIPTION)

    class Meta:
        verbose_name = _('category')
        verbose_name_plural = _('categories')

    def __str__(self):
        return self.name
    
    @classmethod
    def get_random(cls):
        categories_list = list(Category.objects.all())
        random_category = random.choice(categories_list)
        return random_category

        # uuids = cls.objects.values_list('id', flat=True)
        # uuid_random = random.choice(uuids)
        # return str(uuid_random)


class Location(models.Model):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False)

    name = models.CharField(max_length=200, blank=False, null=False, default='location1')
    comment = models.CharField(max_length=300, blank=True)
    image = models.ImageField(upload_to='event_images', null=True, blank=True)

    city = models.CharField(max_length=100, blank=False)
    street = models.CharField(max_length=200, blank=True)
    house_number = models.CharField(max_length=200, blank=True)  # not IntField but CharField for cases as '16/a'

    def __str__(self):
        return self.name
