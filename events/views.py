from django.shortcuts import render
from django.db.models import Q

from django.views.generic import TemplateView, ListView, DetailView
from events.models import Event, Category


# Create your views here.
class IndexPage(ListView):
    template_name = 'index.html'
    queryset = Event.objects.all().order_by('date_time')
    paginate_by = 3

    def get_context_data(self, **kwargs):
        context = super(IndexPage, self).get_context_data(**kwargs)
        context['categories_list'] = Category.objects.all()

        return context


class EventPage(ListView):
    template_name = 'event_details.html'
    queryset = Event.objects.all().order_by('date_time')
    context_object_name = "events_list"
    paginate_by = 1

    def get_context_data(self, **kwargs):
        context = super(EventPage, self).get_context_data(**kwargs)

        event = Event.objects.get(id=self.kwargs.get("pk"))
        context['event'] = event

        if event.category:
            self.queryset = Event.objects.filter(category_id=event.category_id)

        context['categories_list'] = Category.objects.all()
        context['events_list'] = self.queryset.exclude(id=event.id)

        return context


class SearchEvents(ListView):
    template_name = 'search_events.html'
    queryset = Event.objects.all().order_by('date_time')
    context_object_name = "events_list"
    paginate_by = 1

    def get_context_data(self, **kwargs):
        context = super(SearchEvents, self).get_context_data(**kwargs)

        search_text = self.request.GET.get('search')
        events = Event.objects.all()
        text_fields = ["name", "description", "location__name", 'location__city', 'category__name']

        if search_text:
            or_filter = Q()
            for field in text_fields:
                or_filter |= Q(**{f"{field}__icontains": search_text})
            events = Event.objects.filter(or_filter)

        context['search_text'] = search_text
        context['events_list'] = events
        context['categories_list'] = Category.objects.all()

        return context


class CategoryPage(ListView):
    template_name = 'category_details.html'
    queryset = Event.objects.all().order_by('date_time')
    context_object_name = "events_list"
    paginate_by = 1

    def get_context_data(self, **kwargs):
        context = super(CategoryPage, self).get_context_data(**kwargs)

        category = Category.objects.get(id=self.kwargs.get('pk'))

        context['category'] = category
        context['events_list'] = Event.objects.filter(category_id=category.id).order_by('date_time')
        context['categories_list'] = Category.objects.all()
        return context


class ContactsPage(TemplateView):
    template_name = 'contact.html'
    extra_context = {'categories_list': Category.objects.all()}
