from django.urls import path
from events.views import EventPage, CategoryPage, SearchEvents

app_name = 'events'


urlpatterns = [
    path('event/<pk>', EventPage.as_view(), name='event'),
    path('category/<pk>', CategoryPage.as_view(), name='category'),
    path('search/', SearchEvents.as_view(), name='search')
    
]
