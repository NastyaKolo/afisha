from django.contrib import admin
from django.contrib import admin
from .models import Event, Category, Location

# Register your models here.
admin.site.register([Event, Category, Location])
