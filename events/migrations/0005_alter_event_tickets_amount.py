# Generated by Django 3.2.9 on 2021-11-27 11:20

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0004_alter_event_category'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='tickets_amount',
            field=models.IntegerField(blank=True, null=True, validators=[django.core.validators.MinValueValidator(1)]),
        ),
    ]
