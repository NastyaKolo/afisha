# Generated by Django 3.2.9 on 2021-12-01 20:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0006_delete_order'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='media',
            field=models.ImageField(blank=True, default='default.jpeg', null=True, upload_to='event_images'),
        ),
    ]
