from django.shortcuts import render
from events.models import Event
from django.views.generic import TemplateView, UpdateView, ListView, CreateView
from django.contrib.auth.views import LoginView, LogoutView
from users.forms import RegistrationUserForm
from django.urls import reverse, reverse_lazy
from django.contrib.auth import login, authenticate


# Create your views here.
class LoginUser(LoginView):
    template_name = 'registration/login.html'
    success_url = reverse_lazy('index')

    
class LogoutUser(LogoutView):
    template_name = 'registration/logout.html'


class RegistrationUser(CreateView):
    template_name = 'registration/registration.html'
    success_url = reverse_lazy('index')
    form_class = RegistrationUserForm
    
    def form_valid(self, form):
        self.object = form.save(commit=True)
        self.object.is_active = True
        self.object.save()

        user = authenticate(email=self.request.POST['email'],
                            password=self.request.POST['password1'])
        login(self.request, user, backend='django.contrib.auth.backends.ModelBackend')
        return super().form_valid(form)


class UserAccount(ListView):
    template_name = 'user_account.html'
    queryset = Event.objects.all().order_by('date_time')
    context_object_name = "favorite_events"

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(UserAccount, self).get_context_data(**kwargs)

        favorite_events = self.request.user.favorite_events
        context['favorite_events'] = favorite_events
        
        return context
