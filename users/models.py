import uuid

from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from users.managers import CustomManager
from django.contrib.auth import get_user_model

from django.utils.translation import ugettext as _
from django.core.validators import MaxValueValidator, MinValueValidator

from events.models import Event


# Create your models here.
class CustomUser(AbstractBaseUser, PermissionsMixin):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False)

    first_name = models.CharField(_('first name'), max_length=200, blank=True)
    last_name = models.CharField(_('last name'), max_length=200, blank=True)
    email = models.EmailField(_('email address'), max_length=200, blank=False, unique=True)
    
    date_created = models.DateTimeField(_("date joined"), auto_now_add=True)
    is_active = models.BooleanField(_("active"), default=True)
    is_staff = models.BooleanField(_("staff status"), default=False)
    is_superuser = models.BooleanField(_("superuser status"), default=False)

    is_admin = models.BooleanField(_("admin status"), default=False)
    is_editor = models.BooleanField(_("editor status"), default=False)
    is_reader = models.BooleanField(_("reader status"), default=False)

    favorite_events = models.ManyToManyField(to='events.Event', related_name='favorites', null=True, blank=True)

    objects = CustomManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        return self.email


class Order(models.Model):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False)
    date_created = models.DateTimeField(auto_now_add=True)

    customer = models.ForeignKey(to='users.CustomUser', related_name='customer',
                                 on_delete=models.CASCADE, null=False)
    event = models.ForeignKey(to='events.Event', related_name='event',
                              on_delete=models.RESTRICT, null=False)

    tickets_amount = models.IntegerField(blank=False, default=1,
                                         validators=[MaxValueValidator(20), MinValueValidator(1)])

    def total_cost(self):
        return self.event.price * self.tickets_amount

    def __str__(self):
        return f'{self.event}, {self.date_created}'
