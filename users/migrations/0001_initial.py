# Generated by Django 3.2.9 on 2021-11-25 20:59

from django.db import migrations, models
import users.managers
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('events', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomUser',
            fields=[
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, unique=True)),
                ('first_name', models.CharField(blank=True, max_length=200, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=200, verbose_name='last name')),
                ('email', models.EmailField(max_length=200, unique=True, verbose_name='email address')),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='date joined')),
                ('is_active', models.BooleanField(default=True, verbose_name='active')),
                ('is_staff', models.BooleanField(default=False, verbose_name='staff status')),
                ('is_admin', models.BooleanField(default=False, verbose_name='admin status')),
                ('is_editor', models.BooleanField(default=False, verbose_name='editor status')),
                ('is_reader', models.BooleanField(default=False, verbose_name='reader status')),
                ('favorite_events', models.ManyToManyField(related_name='favorites', to='events.Event')),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
            },
            managers=[
                ('objects', users.managers.CustomManager()),
            ],
        ),
    ]
