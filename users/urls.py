from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from users.views import LoginUser, LogoutUser, UserAccount, RegistrationUser

app_name = 'users'


urlpatterns = [
    path('login', LoginUser.as_view(), name='login'),
    path('logout', LogoutUser.as_view(), name='logout'),
    path('registration/', RegistrationUser.as_view(), name='registration'),
    
    path('account/<pk>', UserAccount.as_view(), name='account'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
