python manage.py migrate --settings=afisha_main.settings.${MODE}
python manage.py collectstatic --noinput --settings=afisha_main.settings.${MODE}

python manage.py runserver --settings=afisha_main.settings.${MODE} 0:${WSGI_PORT}
