from celery import shared_task
import requests
from django.core.mail import EmailMessage
from django.http import HttpResponse


def get_currency_exchange():
    response = requests.get("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchangenew?json")
    currency_exchange_content = response.json()
    for currency in currency_exchange_content:
        if currency["cc"] == "USD":
            usd_rate = currency["rate"]
            return usd_rate


@shared_task
def send_current_usd_rate():
    current_usd_rate = get_currency_exchange()
    mail_subject = 'Current USD exchange rate'
    mail_body = f'USD exchange rate to hryvna for today - {current_usd_rate}'
    receiver = 'nastya.kolodezhnaya@gmail.com'
    
    email = EmailMessage(subject=mail_subject, body=mail_body,
                         to=[receiver])

    email.send(fail_silently=False)
