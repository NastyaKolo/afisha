from tasks_celery.tasks import send_current_usd_rate
from django.http import HttpResponse


def send_usd_rate(request):
    send_current_usd_rate.delay()
    return HttpResponse(f'Success, the email was sent to {receiver}')
